package com.example.wcfortablet.main_screen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.wcfortablet.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}