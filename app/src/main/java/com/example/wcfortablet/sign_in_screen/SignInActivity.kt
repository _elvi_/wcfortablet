package com.example.wcfortablet.sign_in_screen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.wcfortablet.R
import com.example.wcfortablet.databinding.ActivitySignInBinding
import com.example.wcfortablet.main_screen.MainActivity

class SignInActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySignInBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySignInBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.enter.setOnClickListener {
            val email = binding.emailEditText.text.toString()
            val password = binding.passwordEditText.text.toString()

            if (email.isNotEmpty() && password.isNotEmpty()) {
                startActivity(
                    Intent(this, MainActivity::class.java)
                )
                finish()
            } else Toast.makeText(this, getString(R.string.stringCheckLines), Toast.LENGTH_SHORT).show()
        }
    }
}